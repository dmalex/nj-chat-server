
const express = require('express')
const app = express()

app.get('/', function(req, res) {
    res.send('<h1>SocketChat Server</h1>')
})

const server = require('http').createServer(app)
const io = require('socket.io')(server)
const port = 3000
var userList = []
var typingUsers = {}

io.on('connection', socket => {
    console.log('A user connected.')

    socket.on('connect', function(){
        console.log('connect')
    })

    socket.on('ping', function(){
        console.log('ping')
    })

    socket.on('chat message', msg => {
        console.log(msg + ' from ' + 'rn client')
        io.emit('chat message', msg) // send to rn client
        const now = new Date().toLocaleString()
        io.emit('newChatMessage', 'rn-clent', msg, now) // send to swift client
        console.log(msg)
    })

    socket.on('chatMessage', function(clientNickName, message){ // from swift client
        console.log(message + ' from ' + clientNickName)

        const currentDateTime = new Date().toLocaleString()
        delete typingUsers[clientNickName]
        io.emit('userTypingUpdate', typingUsers)
        io.emit('newChatMessage', clientNickName, message, currentDateTime) // to swift users
        io.emit('chat message', message + " from " + clientNickName + "@ " + currentDateTime) // to rn client
    })

    socket.on('startType', function(clientNickName){
        console.log('User ' + clientNickName + ' is typing...')
        typingUsers[clientNickName] = 1
        io.emit('userTypingUpdate', typingUsers)
    })


    socket.on('stopType', function(clientNickName){
        console.log('User ' + clientNickName + ' has stopped typing.')
        delete typeUsers[clientNickName]
        io.emit('userTypingUpdate', typingUsers)
    })


    socket.on('connectUser', function(clientNickName){
        var message = 'User ' + clientNickName + ' is connected.'
        console.log(message)

        var userInfo = {}
        var foundUser = false
        for ( var i = 0; i < userList.length; i++ ) {
            if (userList[i]['nickname'] == clientNickName) {
                userList[i]['isConnected'] = true
                userList[i]['id'] = socket.id
                userInfo = userList[i]
                foundUser = true
                break
            }
        }

        if (!foundUser) {
            userInfo['id'] = socket.id
            userInfo['nickname'] = clientNickName
            userInfo['isConnected'] =  true
            userList.push(userInfo)
        }

        io.emit('userList', userList)
        io.emit('userConnectUpdate', userInfo)
    })


    socket.on('exitUser', function(clientNickName){
        for ( var i = 0; i < userList.length; i++ ) {
            if (userList[i]['id'] == socket.id) {
                userList.splice(i, 1)
                break
            }
        }
        io.emit('userExitUpdate', clientNickName)
        console.log('exituser')
    })


    socket.on('disconnect', function(){
        console.log('User disconnected.')

        var clientNickName

        for ( var i = 0; i < userList.length; i++ ) {
            if (userList[i]['id'] == socket.id) {
                userList[i]['isConnected'] = false
                clientNickName = userList[i]['nickname']
                break
            }
        }

        delete typingUsers[clientNickName]
        io.emit('userList', userList)
        io.emit('userExitUpdate', clientNickName)
        io.emit('userTypingUpdate', typingUsers)
    })
})

const showLog = () => {
    console.log('Server is running on port ', port)
}

server.listen(port, showLog)
